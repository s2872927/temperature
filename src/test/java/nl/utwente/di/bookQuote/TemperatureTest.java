package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
// Tests the Quoter

public class TemperatureTest {

    @Test
    public void testTemp() throws Exception {
        Temperature.Temperature1 temper = new Temperature.Temperature1();
        double temp = temper.getFahrenheit("20");
        Assertions.assertEquals(68.0, temp, 0.0, " Temperature in Fahreinheit ");

    }
}