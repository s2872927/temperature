package nl.utwente.di.bookQuote;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Example of a Servlet that gets a Celsius temperature value and returns the value in Fahrenheit.
 */

public class Temperature extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Temperature1 temp;

    public void init() throws ServletException {
        temp = new Temperature1();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Temperature in Fahreinheit: " +
                Double.toString(temp.getFahrenheit(request.getParameter("celsius"))) +
                "</BODY></HTML>");
    }

    public static class Temperature1 {
        public double getFahrenheit(String celsius) {
            double v = (Double.parseDouble(celsius) * (9.0 / 5.0)) + 32.0 ;
            return v;
        }
    }
}
